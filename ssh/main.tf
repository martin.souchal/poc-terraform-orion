# Configuration du provider
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

# Définition du cloud à utiliser
provider "openstack" {
  cloud = "openstack"
}

# Création d'un volume cinder block
resource "openstack_blockstorage_volume_v3" "myvol" {
  region      = "Occitanie"
  name        = "volume_1"
  description = "first test volume"
  size        = 3
}

# Utilisation d'une adresse IP flottante
resource "openstack_networking_floatingip_v2" "floatip_1" {
  pool = "ZONE-PRV"
}

# Définition de l'instance
resource "openstack_compute_instance_v2" "myinstance" {
  name            = "myinstance"
  image_id        = "d0a7f56e-f34e-438f-9f05-5eb19b9e8e6d"
  flavor_id       = "bc12590a-0b23-4147-a12a-4528173650c1"
  key_pair        = "ansible"
  
  network {
    name = "my_first_network_prv"
  }

}

# On attache l'adresse IP flottante
resource "openstack_compute_floatingip_associate_v2" "floatip_1" {
  floating_ip = openstack_networking_floatingip_v2.floatip_1.address
  instance_id = openstack_compute_instance_v2.myinstance.id
  fixed_ip    = openstack_compute_instance_v2.myinstance.network.0.fixed_ip_v4
}

# On attache le volume cinder
resource "openstack_compute_volume_attach_v2" "attached" {
  instance_id = openstack_compute_instance_v2.myinstance.id
  volume_id   = openstack_blockstorage_volume_v3.myvol.id
}

# On configure l'instance avec ssh
resource "null_resource" "ansible" {
  provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo DEBIAN_FRONTEND=noninteractive apt -y upgrade"]
  connection {
      host        = openstack_networking_floatingip_v2.floatip_1.address
      type        = "ssh"
      user        = "debian"
      private_key = file("ansible.pem")
    }
  }

# On configure l'instance avec ansible
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u debian -i '${openstack_networking_floatingip_v2.floatip_1.address},' --private-key ansible.pem ansible/conf.yml"
  }
}