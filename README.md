# Utilisation de Terraform et Ansible sur le cloud Orion

## 2 modes de déploiement

### Avec SSH et Ansible

Dans le dossier ssh/ on se connecte à l'instance une fois qu'elle est accessible en SSH

### Avec cloud-init et Ansible-pull

Dans le dossier ansible-pull/ on commence par mettre à jour l'OS avec cloud-init, puis on installe ansible sur la machine et on récupere le playbook sur git. 

## Récupération des crédentials

Pour vous connecter avec Terraform sur Orion il faut vous identifier avec un token personnel. Pour créer votre token, il faut vous rendre dans votre projet sur la dashboard Horizon, puis dans "Identité", "Identifiants d'application", puis "Créer un identifiant d'application". Télécharger ensuite le fichier "cloud.yaml" et placez le à la racine de ce répertoire. 

Attention à ne pas versionner ce fichier strictement confidentiel.

## Utilisation d'une clé SSH

Il est necessaire d'avoir à la racine du dossier la clé privé qui correponds à la keypair définie dans le fichier main.tf.